<?php get_header();?>	
	<!-- Seçao de Conteudo Inicio-->
		<section id="geral">
			<div class="container">
				<section>
						<div class="widget_body">
							<article>

								<?php if (have_posts()) : while(have_posts()) : the_post();?>

								<div class="news_title">
									<?php the_title();?>
								</div>
							
								<?php  the_content();?>
								
								<?php endwhile;?>
								
								<?php else:get_404_template(); endif;?>
							</article>
							
						</div>
				</section>
				<?php get_sidebar();?>
			</div>
		</section>
	<!-- Seçao de Conteudo Fim-->
<?php get_footer();?>