<?php 


function theme_medicenter(){

	//Chamar a tag Title
	add_theme_support('title-tag');

	//Chama o LogoTipo
	add_theme_support('custom-logo');

}

add_action('after_setup_theme', 'theme_medicenter');

//Registra o Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

//Registra o Menu
register_nav_menus(array(
	'principal'=>__('Menu principal', 'medicenter')
));

//Definir as Miniaturas dos posts
add_theme_support('post-thumbnails');
set_post_thumbnail_size(1280, 720, true);


//Define o tamanho do resumo
add_filter('excerpt_length', function($length){
	return 50;
});

//Define o estilo da Paginação
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes(){
	return 'class="btn btn-info"';
}

//Cria o SideBar
register_sidebar(array(
	'name'=>'Barra Lateral',
	'id'=>'sidebar',
	'before_widget'=>'<div class="card mb-4">',
	'after_widget'=>'</div></div>',
	'before_title'=>'<h5 class="card-header md-3">',
	'after_title'=>'</h5><div class="card-body">',
));

//Ativar formulário para repostas nos comentários

function theme_queue_js(){
	if ((!is_admin()) && is_singular() && comments_open() && get_option('thread_comments')) wp_enqueue_script('comment_reply'); 
}
add_action('wp_print_scripts', 'theme_queue_js');

























