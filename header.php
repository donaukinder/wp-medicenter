<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<meta <?php bloginfo('charset');?>>
	<meta name="viewport" content="width=device-width,user-scalable=0">
	


	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/assets/css/style.css">
  	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/assets/css/bootstrap.css">

	<!-- Font Awesome -->
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	

	<?php wp_head();?>
</head>
<body>
	<!-- Inicio Cabeçalho -->
	<header>
		
		<div class="container">
			<div class="logo">
				<?php

					$custom_logo_id = get_theme_mod('custom_logo');
					$logo = wp_get_attachment_image_src($custom_logo_id, 'full');
					if (has_custom_logo()){
					echo '<img src="' . esc_url($logo[0]) . '">';
				} else {
				}
				 ?>
			</div>
			<div class="menu">
				 <nav class="navbar navbar-expand-lg navbar rounded" role="navigation">
            <div class="container_bs4">
            <!-- Brand and toggle get grouped for better mobile display -->
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    
		    <div></div>
		    <div></div>
		    <div></div>
		  	</button>
              <?php
              wp_nav_menu( array(
                'theme_location'    => 'principal',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'navbarNavDropdown',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker(),
              ) );
              ?>
            </div>
			</nav>
			</div>
		</div>
	</header>
	<!-- Fim Cabeçalho -->
