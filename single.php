<?php get_header();?>
	<!-- Seçao de Banner Inicio -->
	<section id="banner">
		<div class="container column">
			<div class="banner_headline">
				<h1>Um hospital completo para você</h1>
			</div>
			<div class="banner_options">
				<div class="banner1">
					<i class="fa fa-4x fa-user-md"></i>
					<div class="banner_title">Centro Médico</div>
					<div class="banner_desc">Av. tal de tal, 515 – Centro. Manaus – Amazonas – Brasil. Telefone 92 9999-99-999</div>	
				</div>
				<div class="banner2">
					<i class="fa fa-4x fa-hospital-o"></i>
					<div class="banner_title">Pronto Atendimento 24 Horas</div>
					<div class="banner_desc">Av. tal de tal, 515 – Centro. Manaus – Amazonas – Brasil. Telefone 92 9999-99-999</div>
				</div>
				<div class="banner3">
					<i class="fa fa-4x fa-heartbeat"></i>
					<div class="banner_title">Hemodiálise</div>
					<div class="banner_desc">Av. tal de tal, 515 – Centro. Manaus – Amazonas – Brasil. Telefone 92 9999-99-999</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Seçao de Banner Fim -->

	<!-- Seçao de Conteudo Inicio-->
		<section id="geral">
			<div class="container">
				<section>
						<div class="widget_body flex">
							<?php if (have_posts()) : while(have_posts()) : the_post();?>
							<article>
								<div class="news_thumbnail">
									<?php the_post_thumbnail('post-thumbnail', array(
										'class'=>'img-fluid rounded'
									));?>
								</div>
								<div class="news_title">
									<?php the_title();?>
								</div>
							
								<?php  the_content();?>
								
								<div class="news_data">
									<div class="news_posted_at"><?php echo get_the_date('d/m/y');?></div>
								</div>
								<hr>
								<?php comments_template();?>	

							</article>
							<?php endwhile;?>

							<?php else:get_404_template(); endif;?>
							
						</div>
				</section>
				<?php get_sidebar();?>
			</div>
		</section>
	<!-- Seçao de Conteudo Fim-->
<?php get_footer();?>