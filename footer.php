
<!-- Footer Inicio -->
<footer>
	<?php wp_footer();?>
	<div class="container flexColumn">
		<div class="footer_menu">
			<div class="fm_1">
				<div class="fm_1_title">Health Insurance</div>
				<div class="fm_1_text">lorem ipsum dolor sit amet, consectetur</div>
			</div>
			<div class="fm_2">
				<div class="fm_2_title">Medical Records</div>
				<div class="fm_2_text">lorem ipsum dolor sit amet, consectetur</div>
			</div>
			<div class="fm_3">
				<div class="fm_2_title">Online Acess</div>
				<div class="fm_2_text">lorem ipsum dolor sit amet, consectetur</div>
			</div>
		</div>
		<div class="footer_area">
			<div class="footer_areaitem">
				<div class="widget_title">
					<div class="widget_title_text">Medicenter Clinic</div>
					<div class="widget_title_bar"></div>
				</div>
				<div class="footer_areatext">lorem ipsum dolor sit amet, consectetur, lorem ipsum dolor sit amet</div>
				<div class="footer_areatext">Medicenter</div>
				<div class="footer_areatext2">33 Farlane Street</div>
				<div class="footer_areatext2">Keilor East</div>
				<div class="footer_areatext2">VIC 3033, Australia</div>
			</div>
			<div class="footer_areaitem">
				<div class="widget_title">
					<div class="widget_title_text">Latest Posts</div>
					<div class="widget_title_bar"></div>
				</div>
				<div class="post1">
					<p>&rArr; lorem ipsum dolor sit amet, consectetur, lorem ipsum dolor sit amet</p>
					<div class="post1_date">6 years ago</div>
				</div>
				<div class="post1">
					<p>&rArr; lorem ipsum dolor sit amet, consectetur, lorem ipsum dolor sit amet</p>
					<div class="post1_date">6 years ago</div>
				</div>
				<div class="post1">
					<p>&rArr; lorem ipsum dolor sit amet, consectetur, lorem ipsum dolor sit amet</p>
					<div class="post1_date">6 years ago</div>
				</div>
			</div>
			<div class="footer_areaitem">
				<div class="widget_title">
					<div class="widget_title_text">Latest Tweets</div>
					<div class="widget_title_bar"></div>
				</div>
				<div class="post1">
					<p>&rArr; Follow and RT to enter book giveaway! 5 copies of Linux Server Security: Hack and Defend! http://amzn.to/28PkV2h  📚</p>
					<div class="post1_date">6 years ago</div>
				</div>
				<div class="post1">
					<p>&rArr; Follow and RT to enter book giveaway! 5 copies of Linux Server Security: Hack and Defend! http://amzn.to/28PkV2h  📚</p>
					<div class="post1_date">6 years ago</div>
				</div>
				<div class="post1">
					<p>&rArr; Follow and RT to enter book giveaway! 5 copies of Linux Server Security: Hack and Defend! http://amzn.to/28PkV2h  📚</p>
					<div class="post1_date">6 years ago</div>
				</div>
			</div>
		</div>
		<div class="footer_copy">
		Copyright - Nenhum direito reservado - A título de Estudo - 2019
		</div>
	</div>
</footer>
<!-- Footer Fim -->
<script src="<?php bloginfo('template_url');?>/assets/js/jquery.js"></script>
<script src="<?php bloginfo('template_url');?>/assets/js/popper.js"></script>
<script src="<?php bloginfo('template_url');?>/assets/js/bootstrap.min.js"></script>
</body>	
</html>